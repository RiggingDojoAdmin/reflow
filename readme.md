# Reflow

![Reflow screenshot](screen.png)

Reflow is Blender addon that lets you change the fps in a scene.
Instead of using time remapping, Reflow changes the animation data.
It offsets keyframes, markers and fixes NLA lenghts and the scene's endframe.

Time remapping makes timeline scrubbing weird and animations get cut because the
frame's end isn't updated. Changing the keyframes is more "permanent" but avoids
the wonkyness.


## Installing

Download the Python file. Open Blender and go to Edit > Preferences > Add-ons. Click the "Install button" and point to the file you've downloaded. Enable the add-on by clicking the checkbox next to its name.

## Using

Look for the Dimensions panel in the render properties. Click the Change fps button. A new dialog will popup with several settings, including the original fps and the target fps. 

When you are done setting the options click the OK button to make the change, to cancel just move your mouse away from the dialog.

